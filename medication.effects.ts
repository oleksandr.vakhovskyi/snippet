import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';
import { map, skip, switchMap } from 'rxjs/operators';

import * as MedicationActions from './medication.actions';

// tslint:disable-next-line:nx-enforce-module-boundaries
import { ApiService } from '@medopad/shared/services';
import { Medication } from '@medopad/shared/types';

@Injectable()
export class MedicationEffects {
  loadMedication$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MedicationActions.loadMedication),
      fetch({
        run: (request: any) => {
          // Your custom service 'load' logic goes here. For now just return a success action...
          return this.apiService.getMedications(request).pipe(
            map((data: Medication[]) => {
              return MedicationActions.loadMedicationSuccess({
                medication: data,
                request: request.medicationRequest
              } as any);
            })
          );
        },

        onError: (action, error) => {
          return MedicationActions.loadMedicationFailure({ error });
        }
      })
    )
  );

  constructor(private actions$: Actions, private apiService: ApiService) {}
}
