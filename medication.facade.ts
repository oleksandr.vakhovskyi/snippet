import { Injectable } from '@angular/core';

import { select, Store, Action } from '@ngrx/store';

import * as fromMedication from './medication.reducer';
import * as MedicationSelectors from './medication.selectors';
import * as MedicationActions from './medication.actions';

@Injectable()
export class MedicationFacade {
  loaded$ = this.store.pipe(select(MedicationSelectors.getMedicationLoaded));
  medications$ = this.store.pipe(select(MedicationSelectors.getAllMedication));
  errorMedications$ = this.store.pipe(
    select(MedicationSelectors.getMedicationError)
  );

  constructor(private store: Store<fromMedication.MedicationPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
  }

  getMedication(request) {
    this.store.dispatch(MedicationActions.loadMedication(request));
  }
}
