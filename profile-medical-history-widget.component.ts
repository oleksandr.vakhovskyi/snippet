import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { PatientProfileFacade, MedicationFacade } from '@medopad/shared/store';
import {
  AppConfigService,
  ModuleConfigService
} from '@medopad/shared/services';
import { Medication } from '@medopad/shared/types';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import * as moment from 'moment';
@Component({
  selector: 'mp-profile-medical-history-widget',
  templateUrl: './profile-medical-history-widget.component.html',
  styleUrls: ['./profile-medical-history-widget.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileMedicalHistoryWidgetComponent implements OnInit, OnDestroy {
  public isEditState: Boolean = false;
  public startDateTime: string | Date = moment()
    .subtract(1, 'year')
    .utc()
    .format();
  public medicationData: any;
  public isLoading: boolean;
  subscriptions = [];
  patientId: string;

  constructor(
    private appConfigService: AppConfigService,
    private medicationFacade: MedicationFacade,
    private patientProfileFacade: PatientProfileFacade,
    private translateService: TranslateService,
    public moduleConfigService: ModuleConfigService
  ) {}

  ngOnInit(): void {
    this.subscribes();
  }

  getMedications(startDateTime?: string | Date) {
    if (startDateTime) {
      startDateTime = moment(startDateTime)
        .utc()
        .startOf('day')
        .format();
      const request = {
        medicationRequest: {
          limit: 100,
          onlyEnabled: true,
          skip: 0,
          startDateTime
        },
        uid: this.patientId
      };

      this.isLoading = true;
      this.medicationFacade.getMedication(request);
    }
  }

  subscribes() {
    const medicationsSubscription = this.medicationFacade.medications$.subscribe(
      medicationsRes => {
        if (medicationsRes.loaded && medicationsRes.medications) {
          this.isLoading = false;

          const orderedMedications =
            medicationsRes.medications.length > 0
              ? _.orderBy(
                  medicationsRes.medications,
                  ['updateDateTime'],
                  ['desc']
                )
              : [];

          this.medicationData = orderedMedications.map(medication => {
            const scheduleText = this.getFrequencyString(medication);
            return { ...medication, scheduleText };
          });
        }
      }
    );
    const patientIdSubscription = this.patientProfileFacade.patientId$.subscribe(
      patientId => {
        if (patientId) {
          this.patientId = patientId;
          this.getMedications(this.startDateTime);
        }
      }
    );
    this.subscriptions = [medicationsSubscription, patientIdSubscription];
  }

  getTranslatedStr(key: string) {
    const keyId = this.appConfigService.getUnit(key);
    if (keyId) return this.translateService.instant(keyId);
    return '';
  }

  getFrequencyString(medication: Medication): any {
    let isAsNeeded = false;
    if (medication.prn) {
      isAsNeeded = true;
    } else if (medication.schedule) {
      const { frequency, period, periodUnit } = medication.schedule;
      if (frequency && period && periodUnit) {
        let str = '';
        if (frequency === 1) {
          str = this.getTranslatedStr('ONCE');
        } else if (frequency === 2) {
          str = this.getTranslatedStr('TWICE');
        } else {
          str = `${frequency} ${this.getTranslatedStr('TIMES')}`;
        }
        const periodStr = period === 1 ? this.getTranslatedStr('ONE') : period;
        str += ` ${periodStr} ${this.getTranslatedStr(periodUnit)}`;

        return str;
      } else {
        isAsNeeded = true;
      }
    }
    if (isAsNeeded) {
      return this.getTranslatedStr('ASNEEDED');
    }
  }

  ngOnDestroy(): void {
    if (this.subscriptions.length > 0) {
      this.subscriptions.map(subscription => {
        subscription.unsubscribe();
      });
    }
  }
}
