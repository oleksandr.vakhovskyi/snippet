/**
 * Interface for the 'Questionnaire' data
 */

export {
  Medication as MedicationEntity,
  RetrieveMedication
} from '@medopad/shared/types';
