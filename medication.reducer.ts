import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as MedicationActions from './medication.actions';
import { MedicationEntity } from './medication.models';
import { Medication } from '@medopad/shared/types';

export const MEDICATION_FEATURE_KEY = 'Medication';

export interface MedicationState extends EntityState<MedicationEntity> {
  selectedId?: string | number; // which Medication record has been selected
  request?: any;
  medications?: Medication[];
  loaded: boolean; // has the Medication list been loaded
  error?: string | null; // last none error (if any)
}

export interface MedicationPartialState {
  readonly [MEDICATION_FEATURE_KEY]: MedicationState;
}

export const medicationAdapter: EntityAdapter<
  MedicationEntity
> = createEntityAdapter<MedicationEntity>();

export const MedicationInitialState: MedicationState = medicationAdapter.getInitialState(
  {
    // set initial required properties
    loaded: false
  }
);

const reducer = createReducer(
  MedicationInitialState,
  on(MedicationActions.loadMedication, state => {
    return {
      ...state,
      loaded: false,
      error: null
    };
  }),
  on(MedicationActions.loadMedicationSuccess, (state: any, data: any) => ({
    ...state,
    loaded: true,
    medications: data.medication,
    request: data.request
  })),
  on(MedicationActions.loadMedicationFailure, (state, { error }) => ({
    ...state,
    error
  }))
);

export function medicationReducer(
  state: MedicationState | undefined,
  action: Action
) {
  return reducer(state, action);
}
