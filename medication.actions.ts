import { Medication } from '@medopad/shared/types';
import { createAction, props } from '@ngrx/store';
import { MedicationEntity } from './medication.models';

export const loadMedication = createAction(
  '[Medication] Load Medication',
  props<{ request: any }>()
);

export const loadMedicationSuccess = createAction(
  '[Medication] Load Medication Success',
  props<{ loaded: boolean; medications: Medication; request: any }>()
);

export const loadMedicationFailure = createAction(
  '[Medication] Load Medication Failure',
  props<{ error: any }>()
);
