import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  MEDICATION_FEATURE_KEY,
  MedicationState,
  MedicationPartialState,
  medicationAdapter
} from './medication.reducer';

// Lookup the 'Medicatione' feature state managed by NgRx
export const getMedicationState = createFeatureSelector<
  MedicationPartialState,
  MedicationState
>(MEDICATION_FEATURE_KEY);

const { selectAll, selectEntities } = medicationAdapter.getSelectors();

export const getMedicationLoaded = createSelector(
  getMedicationState,
  (state: MedicationState) => state.loaded
);

export const getMedicationError = createSelector(
  getMedicationState,
  (state: MedicationState) => state.error
);
export const getAllMedication = createSelector(
  getMedicationState,
  (state: MedicationState) => {
    return {
      loaded: state.loaded,
      medications: state.medications,
      request: state.request,
      error: state.error
    };
  }
);
